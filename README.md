# Cloud Training

This repository intends to share learning material used by other CapTechers in their career growth and learning journey to the Cloud along with supporting tools & technologies.

As the cloud operates as a horizontal by crossing many areas in any organization, this repo will contain a variety of topics such as programming languages, containers, serverless, machine learning, automation, DevOps, etc.

Everyone is welcome to contribute! The basic guidelines are:
* Only add links and resources that are available to all CapTechers, such as Tech Challenges, Pluralsight training, youtube, etc.
* It is a reference that you used to learn something and not any random link - we all want to learn from each other
* If you find any references that are outdated or irrelevant, please help others by updating the documents
* Don't see a section that can help you? Let's create one!
* Have fun!

The distribution of content is as follows:
* **100** - Getting started training references => This section contains cross-functional skills such as python, bash, API, and anything that can help you get off the ground
* **200** - Beginners => How do I do X, Y, Z?
* **300** - Intermediary => I got it, how do I move it one level up?
* **400** - Advanced => The fun stuff, with lots of moving pieces
* **Certification** - As the name suggests, material for getting certified across Cloud Providers

## Material

### 100 Level - Getting started

* [DevOps and Cloud](100/100-cloud-devops.md)

### 200 Level - Beginners

Coming soon ...

### 300 Level - Intermediary

Coming soon ...

### 400 Level - Advanced

Coming soon ...

### Certifications

* [Azure](certification/certs-azure.md)
* [AWS](certification/aws/README.md)
* [GCP](certification/certs-gcp.md)

# Need Help?

Please reach out to @zhughes or @rbortoloto
