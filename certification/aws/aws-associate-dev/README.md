# AWS Associate Developer Notes

## Content Index

* [Overview](./notes/overview.md)
* [Elastic Compute Cloud](./notes/ec2.md)
* [Identity Access Management](./notes/iam.md)
* [Simple Storage Service](./notes/s3.md)
* [Serverless](./notes/serverless.md)
* [DynamoDB](./notes/dynamodb.md)
* [Key Management Service](./notes/kms.md)
* [Simple Queue Service](./notes/sqs.md)
* [Simple Notification Service](./notes/sns.md)
* [Simple Email Service](./notes/ses.md)
* [Kinesis](./notes/kinesis.md)
* [Elastic Beanstalk](./notes/ebs.md)
* [Continuous Integreation/Continuous Deployment](./notes/cicd.md)
* [CloudFormation](./notes/cloudformation.md)

## Exam Blueprint

### Content Breakdown

* Deployment: 22%
* Security: 26%
* Development with AWS Services: 30%
* Refactoring: 10%
* Monitoring & Torubleshooting: 12%

### Question Breakdown

* Multiple Choice, Multiple Response
* 130 Minutes Total
* 65 Questions
* Passing score of **720/1000**
* Registration $150, Practice Tests $20