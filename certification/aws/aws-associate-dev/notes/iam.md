# Identity Access Management

## IAM 101

IAM allows you to manage users and their level of access to the AWS console. 

IAM gives you:
* Centralised control of your AWS account
* Shared access to your AWS account
* Granular Permissions
* Identity Federation (with Active Directory, Fb, LinkedIn, etc)
* Multifactor Authentication
* Provide temporary access for users/devices and services
* Setup custom password rotation strategy 
* Integration into AWS services
* Support PCI DSS Compliance

### Terminology

* **Users** are end users. Think people
* **Groups** are collections of users who share the same set of permissions
* **Roles** can be created for assigning AWS resources
* **Policies** are documents that define one or more permissions, assigned to users, grorups, and roles. 


### Security Token Service (STS)

* **Federation** means combining or joining a list of uses in one domain (eg. IAM) with a list of users in another domain (such a Active Directory).
* **Identity Brokers** are servides that allow y ou to take an identity from point A and join it (federate it) with point B. 
* **Identity Stores** are services that store identities, like Active Directory or Facebook
* An **Identity** is a user of a service, like Facebook. 


### Federating Active Directory with AWS Console

* You can authenticate with Active Directory using SAML

#### Do you authenticate against Active Directory before or after being given a tempory security credential?

* Always auth against Active Directory first and then you are assigned a security credential


### Web Identity Federation with Mobile Application

You can authenticate mobile apps with services like Facebook, Google, or Microsoft. 

**ARN** is Amazon Resource Name