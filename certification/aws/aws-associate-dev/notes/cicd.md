# CI/CD

## Continuous Integration and Continuous Delivery/Deployment Precursor

CI/CD are best practices for software development and deployment. 

### Workflow

![CI/CD](./images/cicd-devtools.png)

#### Continuous Integration 

* Multiple devs working on different features or bug fixes
    * All contribute to the same application
    * share same code repository
    * frequently push updates to their shared repo
* Code repo is integrated with a **build management system**
    * code changes trigger an automated build
* The test framework runs automated tests on a newly built application
    * It identifies any bugs and pfrevents issues from being introduced into the master code repository
* focuses on small changes in the repository once they have successfully tested

#### Continuous Delivery/Deployment

* CD can mean either continuous delivery or continuous deployment
    * **delivery** is when merged changes are automatically built, tested, prepared for release into staging and eventually production. usually a manual aspect of deployment. 
    * **deployment** automatically deploys new code following the build, test, and package stage. 
        * automates the release process. 

## CodeCommit

* Codecommit is the AWS git offering.
    * centralized repo for all code, binaries, images, and libraries
    * tracks and manages code changes
    * maintains version history
    * manages updates from multiple sources
* Data is encrypted both at rest and in transit.  

## CodeDeploy

CodeDeploy is an automated deployment service which allows you to deploy your application code to EC2, on premise systems, and Lambda functions. 

It automatically scaled with your infrastrcutre and integrated with various CI/CD tools like:
* Jenkins
* Github
* Atlassian
* CodePipeline
* etc

There are two deployment approaches

### In Place

* The application is stopped on each instance and the latet version is installed
    * aka rolling update
    * is not supported on Lambda
* instance is out of service during this time and capacity is reduced
    * can configure load balances to stop sending requests to the upgrading instances
* If you need to roll back changes, you must re deploy the older version of the application

### Blue/Green

* New instances are provisioned and the latest version is installed on the new instances. 
* New instaled are registered with elastic load balances, and traffic is then rerouted to the new instances
* old instances are eventually terminated. 
* Code can be released to production by simplly rerouting traffic, making it easier to revert a change as well. 

### Terminology

* **Deployment Group**s are a set of EC2 instances or Lambda functions to which a new revision of the software is to be deployed. 
* **Deployment** is the process and components used to apply a new version
* **Deployment Configuration**s are a set of deployment rules as well as success/failure conditions used for deployment
* **AppSpec File**s determine the deployment actions you want CodeDeploy to execute
* **Revision**s are everything needed to deploy a new version, including the AppSpec file, application files, executables, and config files. 
* **Application**s are the unique identifier for the application you want to deploy. To ensure the correct combination of revision, deployment configuration, and deployment group are referenced during deployment. 

### The AppSpec File

The AppSpec file is used to dfine the parameters that will be used for CodeDeploy deployment. The file structue is dependent on the deployment enviornment: Lambda, EC2, or on premise. 

#### Lambda Deployments

The file may be written in YAML or JSON and contains the following fields
* **version** is reserved for future use. currently the only allowed version is 0.0
* **resources** are the name and properties of the Lambda function to deploy. 
* **hooks** specify lambda functions to run at set points in the deployment lifecycle to validate the deployment

##### Example

```YAML
version: 0.0
resources:
    - myLambdaFunction:
        Type: AWS::Lambda::Function
        Properties:
            Name: "myLambdaFunction"
            Alias: "myLambdaFunction"
            CurrentVersion: "1"
            TargetVersion: "2"
hooks:
    - BeforeAllowTraffic: "LambdaFunctionToValidateBeforeTrafficShift"
    - AfterAllowTraffic: "LambdaFunctionToValidateAfterTrafficShift"
```

#### EC2 and On Premise

`appspec.yml` must be placed in the **root directory of your revision**. This is the same directory containing application source code. 

* **version** reseved for future use, only allowed 0.0
* **os** defines the operating system, like linux or windows. 
* **files** defines the location of any application files that need to be copied, and where they should be copies to
* **hooks** are lifecycle events that allow you to specify scripts that need to run at set points in the deployment lifecycle. 

![EC2 AppSpec Hooks Lifecycle](./images/ec2-lifecycle-appspec.png)

##### Example

```YAML
version: 0.0
os: linux
files: 
    - source: Config/config.txt
      destination: /webapps/Config
    - source: Source
      destination: /webapps/myApp
hooks:
    BeforeInstall:
        - location: Scripts/unzipit.sh
    AfterInstall:
        - location: Scripts/runtests.sh
    ApplicationStart:
        - location: Scripts/runfunctionaltests.sh
    ValidateService:
        - location: Scripts/validateservice.sh
```

## CodePipeline

* Bundled Continuous Integration/Continuous Delivery service
* Automates end to end software release process based on a defined workflow
* Can be configured to trigger the pipeline as soon as a change is detected in your source code repo
* Integrated with other services from AWS like CodeBuild and CodeDeploy, as well as other 3rd party plug ins. 
