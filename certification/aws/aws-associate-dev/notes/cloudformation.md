# CloudFormation

CloudFormation is a service that allows you to manage, configure, and provision your AWS infrastrcture as code. Resources are defined using a CloudFormation template

## CloudFormation Template

* YAML or JSON describes the endstate of the infrastructure you are changing or provisioning
* After creation, upload the template to CloudFormation using S3
* CloudFormation reads the template and makes the API calls on your behalf
    * The resulting resources are called a **stack**
* Resources is the only mandatory section of CloudFormation Template
* Transform section is used to reference additional code stored in S3 allowing for code reuse

### Example

```YAML
AWSTemplateFormatVersion: "2010-09-09"
Description: "Template to create an EC2 instance"
Metadata:
    Instances:
        Description: "Web Server Instance"

Parameters: # input values
    EnvType:
        Description: "Environment type"
        Type: String
        AllowedValues:
            - prod
            - test
Conditions:
    CreateProdResources: !Equals [ !Ref EnvType, prod ]

Mappings: # eg. set values based on a region
    RegionMap:
        eu-west-1:
            "ami": "ami-obdb1d6c15a40392c"

Transform: # include snippets of code outside of the main template
    Name: 'AWS::Include'
    Parameters:
        Locations: 's3://MyAmazonS3BucketName/MyFileName.yaml'

Resources: # The resources you are deploying
    EC2Instance:
        Type: AWS::EC2::Instance
        Properties:
            InstanceType: t2.micro
            ImageId: ami-obdb1d6c15a40392c

Outputs:
    InstanceID:
        Description: The Instance ID
        Value: !Ref EC2Instance
```

## Serverless Application Model

SAM is an extension to CloudFormation used to define serverless application. It has a simplified syntax for definition serverless resources like APIs, Lambda functions, DynamoDB tables, etc. You can use the SAM CLI to package your deployment, upload it to S3, and deployt to serverless applications. 

### Commands

#### Packaging

```
$ sam package --template-file ./mytemplate.yml -- output-template-file same-template.yml --s3-bucket s3-bucket-name
```

#### Deploying

```
sam deploy --template-file sam-template.yml --stack-name mystack --capabilities CAPABILITY_IAM
```