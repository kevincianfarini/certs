# Simple Queue Service

## SQS 101

SQS is a web service that gives you access to a message queue that can be used to store messages while waiting for a computer to process them. 

SQS is a **distributed queue system** that enables web application to quickly an reliably queue messages that one component in an application generates, which is then consumed by another component. 

Ultimately, it's a temeporary repository for messages that are awaiting processing. 

## Architecture

* SQS is a **pull** based system. Component must poll the queue for new jobs. 
* Good for decoupling components of an application so they run independently. 
* Messages can contain up to 256KB of text in any format
* Gaurentees that messages will be delivered at least once
* Comoponents can retrieve messages programatically using the SQS API
* Messages can be kept in queue from 1 minute all the way up to 14 days
    * default is 4 days

## Benefits

SQS acts as a buffer between the component producing data and the component recieving data. This resolves the issue of work being produced faster than the consumer can process it, or if the connection to the network for the producer and consumer is intermittent. 

## Queue Types

### Standard Queue (Default) 

* Allows you to have nearly unlimited transactions per second. 
* Guarentees that a message is delivered at least once
    * However, more than one copy may be delivered out of order because of the distributed nature
* offers best effort ordering which ensure that messages are **generally** delivered in the same order they are sent. 

### FIFO Queue

* Compliments the standard queue
* biggest features are ordered delivery (FIFO) and exactly once processing
    * the order in which messages are sent and delivered is strictly preserved
    * a messages remains available until a consumer processes and deletes it
* 300 transactions per second

## Visibility Timeout

* The amount of time that a message is invisible in the queue after a consumer picks up the message. 
    * If the messages is successfully processed and deleted before the visibilty timeout, not other consumers can pick it up. 
    * if it's not processed and deleted while invisibile, another consumer can pick up the job, thus resulting in the same message being delivered twice. 
* Default timeout of 30 seconds, max timeout of 12 hours. 
* Changes to message visibility by making a call to the `ChangeMessageVisibility` API. 

## Polling

### Short Polling

* Returns immidiately, whether or not a message is in the queue

### Long Polling

* An alternative way to retrieve messages from SQS queues. 
* Long polling doesn't return immidiately (even if the queue being polled is empty). It does not return a response until a message is available in the queue, or the long poll times out. 
* Long polling can save you money by lessening the amount of requests to SQS. 
* Max long poll timeout is 20 seconds