# Key Management Service

## Introduction

KMS is a managed service that makes it easy for you to create and control encryption keys used to encrypt your data. It's integrated into other services, including Amazon Redshift, Amazon Elastic Transcoder, Amazon Workmail, Amazon RDS, and more. 

**KMS is regional, not global**

## The Customer Master Key

Customer master key consists of:
* alias name
* creation date
* description
* key state
* key material
    * either customer provided or AWS provided

The CMK can never be exported from AWS. 

## KMS API Calls

### Encrypt a File

```
$ aws kms encrpyt --key-id {YOUR_KEY_ID_HERE} --plaintext fileb://{FILENAME}.txt --output text --query CiphertextBlob | base64 --decode > {ENCRYPTED_FILENAME}.txt
```

### Decrypt a File
```
$ aws kms decrypt --ciphertext-blob fileb://{ENCRYPTED_FILENAME}.txt --output text --query Plaintext | base64 --decode > {DECRYPTED_FILENAME}.txt
```

### Re-Encrypt a File
```
$ aws kms re-encrypt --destination-key-id {YOUR_KEY_ID_HERE} --ciphertext-blob fileb://{ENCRYPTED_FILENAME}.txt | base64 > {NEW_ENCRYPTED_FILENAME}.txt
```

### Enabling Key Rotation

Key rotation rotates a key every single year 
```
$ aws kms enable-key-rotation --key-id {YOU_KEY_ID_HERE}
```

## Envelope Encryption

### Terminology

* **Envelope (data) key** is the key that is used to decrypt/encrypt your data. 

### Process

Envelope encryption is encrypting the key that encrypts your data. The customer master key (CMK) is used to decrypt the envelope (data) key. The data key is then used to decrypt the desired data. 

![Envelope Encryption](./images/envelope-encryption-kms.png)