# Elatic Compute Cloud

## EC2 101

stands for Elastic compute cloud. provides resizable compute capacity in the cloud, allows for quickly scaling as your requirements change. Changed the economics of coud computing. Pay for the resources you actually use versus overprovisioning. 

### Options

* **On Demand** provides a fixed rate on a hour or second rate. Linux instances by the hour, windows instances by the second. 
* **Reserved** is like entering into a contract with AWS. Significant discount over hourly use, but 1 to 3 years term. 
* **Spot** is bidding for resources which allows for possible cheap instancing. Good for applications that have a flexible timeframe. 
* **Dedicated Hosts** are physical hosts that are dedicated for your use. Allows you to lessen costs with server bound software costs. Eg VmWare

### On Demand Pricing

Good for:
* applications with short term, spiky, or unpredicatable workloads. 
* Applications being developed or tested on EC2 for the first time. 

### Reserved Instances

Good For: 

* Applications with steady state or predictable usage
* Applications that require reserved capacity
* Users make up front payments to reduce their total computing costs even further
* up to 75% off on demand prices

### Spot Instances

* Applications that have flexible start and end times
* Applications are only feasible at very low compute prices. 
* Basically good for non peak hours

### Dedicated Hosts

* Useful for regulatory requirements that may not support multi-tenant virtualization. 
* Great for licensing which does not support multi tenancy or cloud deployments
* Can be purchaed on demand (hourly)
* Can be purchased as a reservation for up to 70% off on demand pricing. 

## Elastic Block Store

### What is EBS?

EBS is a virtual disk. Stands for elastic block storage. Attach storage to an EC2 instance.

Places in specific **AZ**s where they are automatically replicated to protect you from the failure of a single component. 

#### EBS Volume Types

* ***SSD***
    * **General Purpose SSD** (GP2)
        * Balances price and performance
        * Ratio of 3 IOPS per GB with up to 10,000 IOPS and the ability to burst up to 3000 IOPS for extended periods of time for volumes at 3334 GiB and above. 
    * **Provisioned IOPS SSD** (IO1)
        * Designed for I/O intensive applications such as large relational or NpSQL databases. 
        * Typically used if you need more than 10,000 IOPS. 
        * Can provision up to 20,000 per volume. 
* ***Magnetic***
    * **Throughput Optimized HDD** (ST1)
        * Big data
        * Data warehousing
        * Log processing
        * *Cannot* be a boot volume
    * **Cold HDD** (SC1)
        * Lowest cost storage for infrequently accessed workloads
        * Eg. File Server. 
        * *Cannot* be a boot volume
    * **Magnetic** (Standard)
        * Legacy
        * Lowest cost per gigabyte of all EBS volume types that is bootable. 


## Security Groups

A security group is a virtual firewall. An EC2 Instance can have one or more security groups. 

* Any rule change you make to a security group applies **immidiately**
* security groups are **STATEFUL**, meaning that inbound rules are also allowed outbound. 
    * Eg. If a request can hit a web server, the server will still be able to respond via the same rule. 
* All inbound traffic is blocked by default
* All outbound traffic is allowed by default
* Security groups have a many to many relationship with EC2 instances
* You cannot block specific IP addresses using security groups. You should instead use Netwrok Access Control Lists
* Can specify allow rules, but not deny rules

## EBS Volumes

* EBS volumes will **always** be in the same availability zone as the EC2 instance
* Volumes exist on EBS and are virtual hard disks. 
* Snapshots exist on S3
* Snapshots are point in time copies of Volumes
    * Snapshots are incremental, meaning only recording changes. 
* To create a snapshot for Amazon EBS volumes that serve as root devices, you should probbly stop the instance before taking the snapshot. 
    * However, you *can* take a snap while the instance is running. 
* Amazon Machine Images can be created from both Volumes and Snapshots. 
* EBS volume sizes and storage types can be changes on the fly
* In order to move an EC2 volume from one AZ/Region to another, take a snapshot or image of it, then copy it to the new AZ/Region. 
* You can share snapshots, but only if they're unencrypted. 

## EFS (Elastic Filesystem)

* File storage service for EC2
* Storage capacity is elastic, grows and shrinks as needed. 
* Features:
    * Supports network file system version 4 (NFSv4)
    * Pay as you go, no pre provisioning required. 
    * scales up to the petabytes
    * support for thousands of concurrent NFS connections
    * Data is stored accross multiple AZs within a region
    * Read after write consistency
    * Block based storage, not object based storage. 
* EFS is essentially used as a file server for a centralized file server
    * All your EC2 instances can access the same files for use
    * security on EFS would propegate to all services dependent on it

## Instance and User Metadata

In order to get your instance's public IP 
```bash
$ curl http://169.254.169.254/latest/meta-data/pubic-ipv4
```

## Elastic Load Balancers

* Instances monitored by ELBs are reported as InService or OutofService.
    * Service status is determined by the 'health' of the service
* Health checks check the instance health by pinging a certain file on the instance
* They have their own DNS names. AWS users are never given the IP address. 


## SDK Exam Tips

Available SDKs:

* Android, iOS, Javascript (Web)
* Java
* .NET
* Node.js
* PhP
* Python
* Ruby
* Go
* C++

Default Regions:

* US-East-1 for most SDKs

Some SDKs have a default region, some do not. Java does, Node.js doesn't. 

## EC2 Summary

Know the difference between:

* On Demand Pricing
* Spot Pricing
    * Remember: If you terminate the instance the amount of hours you pay rounds up. If Amazon terminates it, the pricing rounds down to the nearest hour. 
* Reserved pricing
* Dedicated host pricing

EBS Consists of:

* SSD, general purpose (up to 10,000 IOPS)
* SSD, provisioned IOPS (More than 10,000 IOPS)
* HDD, Throughput optimized (frequently accessed workloads)
* HDD, Cold (less frequently accessed storage)
* HDD, Magnetic, cheap (infrequently accessed storage)

**Remember**: you cannot mount one EBS volume to multiple EC2 instances. Instead, use EFS.

Cloudwatch is for performance monitoring
CloudTrail is for auditing 