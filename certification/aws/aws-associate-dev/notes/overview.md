# Overview

## Global Infrastructure

* **Availability Zones** are synonymous with data centers.
* **Regions** are geographical areas that consist of two or more **AZs**.
* **Edge Locations** are Amazon's localized CDN that are used for caching static content. There exists more edge locations than regions to lower latency for media delivery. 

## AWS Compute

* **EC2 (Cloud Compute)** are VMs in the cloud patform. 
* **EC2 Container Services** are docker container hosts. 
* **Elastic Beanstalk** has automated provisioning.
* **Lambda** is a service that runs code on tiggers agnostic of operating system. 
* **Lightsail** VPS service. Provision you with a server and fixed IP provides RDP and SSD access. 
* **Batch** for batch computing. 

## AWS Storage

* **AWS S3** Simple Storage Service. Object based storage. Files are uploaded into buckets. 
* **EFS (Elastic File System)** NAS. Mount this storage to multiple virtual machines. 
* **Glacier** is for data archival. 
* **Snowball** used for transmitting large amounts of data into AWS. Physically import data via hardware storage. 
* **Storage Gateway** virtual machines that replicate data back to AWS. 

## Databases

* **RDS (Relational Database Service)** service that runs relational databases. 
* **DynamoDB** for non relational databases. 
* **Elasticache** is used for cachine commonly queried items from your database service. 
* **Red Shift** data warehousing. Used for complex queries. 

## Migration

* **AWS Migration Hub** is a tracking service that tracks your applications as you migrate them to AWS. 
* **Application Discovery Service** detects what applications you have, and also depects their dependencies. 
* **Database Migration Service** makes it easy to migrate data from on premise hosting into AWS. 
* **Server Migration Service** helps you migrate virtual and physical servers to AWS. 
* **Snowball** *See Above*

## Networking and Content Delivery

* **VPC** amazon virtual private cloud. Virtual datacenter. Configure firewalls, availability zones, etc. 
* **CloudFront** Amazon's content delivery network. Serves cached static assets. 
* **Route53** Amazon's DNS service. 
* **API Gateway** is a way of creating your own APIs for your service to talk to. 
* **Direct connect** runs a dedicated line from your resources to your VPC in AWS. 

## Developer Tools

* **CodeStar** gets groups of developers together for working on projects. Service for collab with other devs. 
* **CodeCommit** is a source control service. Store your own git repos in here. 
* **Codebuild** will compile your code for you and build a deployment package. 
* **CodeDeploy** will deploy your builds either in AWS or on premise. 
* **Cloud9** is an IDE environment.

## Management Tools

* **Cloudwatch** is a monitoring service for sysops. 
* **CloudFormation** is a way of scripting infrastructure that was previously hardware based (eg firewall). 
* **CloudTrail** logs changes to your AWS environment. 
* **Config** monitors configuration of your entire AWS environment and takes snapshots for easy reversion. 
* **OpsWorks** is a method for automating your environment. Similar to **ElasicBeanstalk** but is more robust. 
* **Service Catalog** manages a catalog of IT services that approve the use of AWS services. 
* **Systems Manager** is an interface for managing AWS resources. 
* **Trusted Advisor** gives you advice accross multiple different disicplines (eg security, performance). For example, if you leave a port open. 
* **Managed Services** for when you don't want to have to worry about scaling or managing your services. 

## Media Services

* **Elastic Transcoder** transcodes media. 
* **MediaConvet** file based video transcoding for broadcasting
* **MediaLive** broadcast grade live video streaming services. 
* **MediaPackage** prepares media for delivery over the internet
* **MediaStore** storage optimized for media
* **MediaTailor** targeted advertizing into video streams. 


## Machine Learning

* **SageMaker** makes it easier for devs to use deep learning
* **Comprehend** does sentiment analysis using data
* **DeepLens** is image recognition on steroids. Physical peice of hardware. 
* **Lex** powers amazon Alexa service. Way of communicating with customers using AI. 
* **Machine Learning** is machine learning in AWS, duh. 
* **Polly** takes text and turns it into speach. 

## Analytics

* **Athena** runs sql queries against things in your S3 buckets. Eg. query through excel files. 
* **EMR (Elastic Map Reduce)** is used for processing large amounts of data. Parallel processing. 
* **CloudSearch and ElasticSearch** are search services for AWS. 
* **Kinesis** is for big data processing. Is a way of ingesting large amounts of data into AWS, eg social media feeds. 
* **Quicksight** is Amazon's BI tool
* **Data Pipeline** is desgined to move your data through different AWS services. 
* **Glue** is used for ETC (Extract, Transform, Load). 

## Security, Identity, and Compliance

* **IAM (Identity Access Management)** 
* **Cognito** is used for device authentication. Use cognito service to request access to AWS resources. 
* **GuardDuty** monitors for malicious activites on your AWS account. 
* **Inspector** is installed on a VM. Use it to run tests to find security vulnerabilities. 
* **Macie** scans your S3 buckets for PII
* **Certificate Manager** dolls out SSL certs through AWS
* **Cloud HSM** are dedicated bits of hardware used to store private and public keys for accessing encrypted information and service instances. 
* **Directory Service** is a way of integrating Microsoft Active Directory with AWS
* **WAF (Web Application Firewall)** looks at the application layer and monitors what a user is doing for things like SQL injection. 
* **Shield** DDoS mitigation
* **Artifact** for compliance reports. 

## Mobile Services

* **Mobile Hub** is used for connecting AWS with mobile apps with AWS SDK
* **Pinpoint** uses targeted push notifications for user engagement. 
* **AWS Appsync** automatically updates data in mobile and web applications
* **Device Farm** is a way to test apps on real physical devices. 
* **Mobile Anaylytics** is an analytics services for mobile devies. 

## Application Integration

* **Step Functions** manages different lambda functions
* **MQ** is used for message queues
* **SNS** is a notification service
* **SQS** is a way of decoupling infrastructure. 
* **SWF** is a workflow service. 

## Customer Engagement

* **Connect** Contact center as a service.
* **Simple Email Service** is used for sending emails, great for sending large amounts of emails. 

## Business Productivity

* **Chime** is video conferencing for AWS. 
* **Work Docs** is a way to safely and securly store work docs. 
* **WorkMail** is amazons email providing. 

## Desktop and App Streaming

* **WorkSpaces** is a VDI solution. Run an OS in the cloud and stream the desktop env to your device. 
* **AppStream 2.0** is a way of streaming just the application
