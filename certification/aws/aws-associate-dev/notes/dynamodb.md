# DynamoDB

## Introduction

* DynamoDB is a fast and flexible NoSQL database for all applications that need consistent, single-digit millisecond latency at any scale. 
* Supports both document and key-value data models
* Data housed in SSD storage
* Data is stored across 3 geographically distinct data centers
* Provides a choice of 2 consistency models:
    * Eventual Consistent Reads **(Default)**
    * Strongly Consistent Reads

## Consistency Models

### Eventually Consistent Reads

* Consistency across all copies of data is usually reached within a second. Repeating read ater a short time should return the updated data. 
* This provides the best performance for DynamoDB

### Strongly Consistent Reads

* Returns a result that reflects all writes that recieved a successful response prior to the read. 

## Structure

The DynamoDB structure consists of the following:
* Tables
* Items (row of data in the table)
* Attributes (Column of data in a table)
* supports:
    * key-value data storage
        * **key** is name of data, **value** is the data itself
    * document data structure
        * can be written in JSON, HTML, or XML

## Primary Keys

DynamoDB stores and retrives data based on a **Primary Key**. There are two types of primary keys. 

* **Parition Key**s are unique attributes (eg. a user ID)
    * this value is provided as input to an internal hash function which determines the partition (physical location) where the data is stored. 
    * If using partition key as a primary key, then no two items can have the same parition key. 
* **Composite Key**s are a combination of a **Partition Key** and **Sort Key**. (eg. a user posting on a forum thread multiple times)
    * Partition Key - User ID
    * Sort Key - Timestamp of forum post
    * This allows you to store multiple items with the same partition key

## Access Control

* Managed via IAM
* Can generate an IAM user within your AWS account with specific permissions to access and create DynamoDB tables. 
* Can generate IAM role which enabled you to obtain temporary access keys that can be used to access DynamoDB
* Use a special **IAM Condition** to restrict user access to only their own records. 

## Indexing

In SQL, an index is a data structure which allows you to perform fast queries on specific columns in a table. You run searched on an index rather then the dataset. 

In DynamoDB, 2 types of indexes are supported to help speed up queries. 
* Local Secondary Index
* Global Secondary Index

![Indexing DynamoDB](./images/indexes-dynamodb.png)

### Local Secondary Index

* Can only be created during table creation
    * cannot add, remove, or modify it later
* has the same partition key as your original table, but a different sort key
* Provides a different view of your data, organized according to an alternative sort key
    * any queries based on this alternative sort key as much quicker than using the index in the main table

### Goobal Secondary Index

* Much more flexible than Local Secondary Index
    * can be created during table creation, or added later
    * both different partition key as well as sort key
* speeds up queries that are related to the alternative partition and sort key

## Querying

### Query Operation

* A **query** operation finds items in a table based on the Primary Key attribute and a distinct value to search for. 
    * use an optional sort key name and value to refine and filter the results
    * A **ProjectionExpression** is used to return only specific item attributes you want from a query
    * Results are always sorted by the sort key
        * can reverse by setting **ScanIndexForward** param to `false`.
    * By default, all queries are **eventually consistent** with the option to be **strongly consistent**

### Scan Operation

* A **scan** operation examines every item in the table. 
    * By default it returns all data attributes, but you can use a **ProjectionExpression**. 
* Scans **do not** search the database. 
    * They dump the entire table and then apply a filter to the dumped data. (This adds an extra step)
* As tables grow larger, scan operations take longer. 
* A scan operation on a large table can use up the provisioned throughput for a large table in just a single operation. 

In general, try and avoid using scan.

### Scan Vs. Query

* **Query** operation finds items in a table using only the PK attribute
    * can provide PK name and distinct value to search for
* **Scan** operations examine everything in the table
* Query is generally more efficient than scan


## Provisioned Throughput

Provisioned throughput is the mechanism in which we define capacity and performance requirements in DynamoDB. 

### Capacity Units

DynamoDB Provisioned Throughput is measued in **Capacity Units**. 

* Specify your requirements in terms of Read and Wite Capacity Units
    * 1 **Write Capacity Unit** == A single 1KB write per second
    * 1 **Read Capacity Unit**:
        * 1 **Strongly Consistent** read of 4KB per second
        * ***OR***
        * 2 **eventually consistent** reads of 4KB per second (DEFAULT)

#### Example Configuration

* Table with 5 Read Capacity Units and 5 Write Capacity Units performs
    * 5 * 4KB Strongly Consistent reads == *20KB per second*
        * Twice as many eventually consistent, *40KB per second*
    * 5 * 1KB write == *5KB per second*


### Calculating Required Capacity Units

* Your application needs to read 80 items (table rows) per second
    * Each item is 3KB in size
    * need strongly consistent reads

First, calculate how many read capacity units are needed for each read. 

```
3KB / 4KB == .75KB
```

Round up, `1KBps`. 

Multiply by the number of reads per second, resulting in **80 Capacity Units** required. For evenetually consistent, divide by two == **40 Capacity Units**. 

## DynamoDB Accelerator (DAX)

DAX is a fully managed, clustered in memory cache for DynamoDB. 

* Delivers up to a 10x read performance improvement
* Microsecond performance for millions of requests per second
* ideal for read heavy and burst workloads
* reduces the read load on database tables, and you may be able to reduce read capacity units. 

### How It Works

DAX is a write through cache, meaning that data is written to the cache as well as the database at the same time. 

* API calls can be directed towards DAX. 
    * When they item you're querying is in the cache, the result is returned from the cache. This is called a **cache hit**
    * When it's not in the cache, DAX performs an *eventually consistent* `GetItem` operation against database. This is called a **cache miss**
        * Item fetched is also written into the cache

### Use Cases

DAX is useful for:
* **eventually consistent** reads only
* Read heavy, write sparse applications
* aplpications that require microsecond response times


## ElastiCache

Suitable for use in front of RDS and DynamoDB instances. DAX in contrast is for use only with DynamoDB. 

**ElastiCache** is an in memory cache in the cloud. It:
* Improves performance of web application, allowing you to retrieve information from fast in memory caches. 
* Sits in between your application and the database
* takes the load off your database
* good if your database is particularly read-heavy and not changing frequently. 

There are two types of ElastiCache. 

### Memcached

* Widely adopted
* Multi threaded
* **NO** multi AZ capability

### Redis

* Open source in memory key-value store
* supports more complex data structures
    * sorted sets
    * lists
* supports master/slave replication and multi-AZ for redundancy

### Caching Strateies

#### Lazy Loading

* Loads data into cache when necessary
* if the data is:
    * **in the cache**, ElastiCache returns the data to the application
    * **not in the cache**, Elasticache returns `null`. Then fetches data from database and writes it into the cache for future use. 
* Stale data can be dealt with using a **Time To Live (TTL)**
    * specifies number of seconds until data in cache expires
    * expired data is treated as cache miss and causes the application to re-retrieve the data
    * doesn't eliminate stale data, but helps avoid it.

![Lazy Loading](./images/lazy-loading-elasticache.png)

#### Write Through

* Adds or updates data to the cache whenever data is written to the database

![Write Through](./images/write-through-elasticache.png)