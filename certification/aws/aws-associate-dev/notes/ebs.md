# Elastic Beanstalk

## Elastic Beanstalk 101

Elastic Beanstalk is a service for deploying and scaling web applications onto widely used application servers like `Apache Tomcat`, `Nginx`, `Passenger`, and `IIS`. 

It supports technologies including:

* Java
* .NET
* PHP
* Node.js
* Python
* Ruby
* Go
* Docker

You upload the code, and elastic beanstalk will handle deployment, provisioning, load balancing, auto scaling, and application health. 

You are given the ability to manage the EC2 instances yourself or let elastic beanstalk take control. 

## Deployment Policies

### All At Once

* Deploys new version to all instances at the same time
* All instances out of service while update is in progress
    * thus making an outage during the update
* if the update fails, you need to manually roll back by re deploying the previous version

### Rolling

* Deploys new update in batches
    * each batch of instances is out of service while deployment takes place
    * capacity is reduced by the number on instances down for update at a given time
* not ideal for performance sensitive systems
* If the update fails, you will need to provide an additional rolling update to roll back to the previous version

### Rolling With Additional Batch

* Launches an additional batch of instances during an update
* deploy new version in batches
* maintains full capacity during deployment
* If the update fails, you need to perform an additional rolling update to roll back

### Immutable

* Deploys a new version to a fresh group of instances in their own autoscaling group
* when the new instances pass their health checks, they are moved to your existing auto scaling group and the old instances are terminated
* maintains full capacity during deployment
* a failed update only required terminatin the new auto scaling group
* preferred for mission critial production systems

## Advanced Elastic Beanstalk

You can customer elastic beanstalk environment using config files. Files are written in YAML or JSON, but must have a `.config` extension and be saved in a folder called `.ebextensions`. 

The `.ebextensions` folder must be in the top level directory of you application source code bundle. 

## RDS Integration

There are two ways of integrating RDS into beanstalk. 

### Suited for Dev and Test

Launching RDS within the beanstalk instance console means that this RDS instance is created within your beanstalk environment. This is a good option for Dev and Test deployments. 

This is not good for production though because the lifecycle of the database if then tied to the lifecycle of your application environment. 

### Suited for Production

The preferred option for production is to decouple the RDS instance from the EBS environment. This is done by launching the RDS instance directly from the RDS console. 

This allows you more flexibility by giving you the ability to connect multiple environments to the same database, provides a wider choice of database types, and allows your database to live independently of your application lifecycle. 

#### Access to RDS from EBS

To allow the EC2 instances in your EBS environmentt to connect to an outside databse, you need to:

* add an additional security group to your environments auto scaling group
* provide connection string config information to your application servers