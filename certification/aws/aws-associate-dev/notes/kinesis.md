# Kinesis

## Precursor: Streaming Data

**Streaming data** is data that is generated continuously by thousands of data sources, which typically send the data record simultaneously and in small sizes (kilobytes). Eg:
* purchases from online stores
* stock prices
* game data

## Kinesis 101

Kinesis is a platform on AWS to send your streaming data to. It makes it easy to load, analyze, and build applications around your streaming data. 

## Services

### Kinesis Streams

1. Data is sent from **producers** into a streams' shards. 
2. The shards store the data anywhere between 24 hours and 7 days
3. **Consumers** read from shards and do something useful to it (eg. sentiment analysis)
4. The result of the consumers can be send to other AWS services

![Kinesis Streams Arch](./images/kinesis-streams-arch.png)

* Kinesis streams consist of **shards**
    * A shard gives 5 transactions per second for reads, up to a max total data rate of 2MB per second as well as 1,000 records per second for writes up to a max of 1MB per second
* The data capacity of your stream is dependent on the number of shards that you allocate to your stream. 
    * total capacity is the sum of the shard capacities

### Kinesis Firehose

* Firehouse is similar to streams, except that Firehouse lacks shards. 
* Data can be analyzed using Lambda **within** firehouse, and written to either:
    * S3
    * S3 then Redshift
    * ElasticSearch Cluster