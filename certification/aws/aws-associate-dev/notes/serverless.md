# AWS Serverless

## Lambda
Lambda is a compute srvice based on "functions" that run code. Provisioning and managing servers is abstracted away from the user, meaning you don't worry about anything aside from the code you run. 

It can be used in the following ways:

* An event driven service that resonds to events (eg. change in a DynamoDB table)
* A compute service in response to HTTP requests using AWS  API Gateway. 

### Lambda automatically scales
meaning that on every invocation of a Lambda function, the same code is run, however it is run in parallel in another Lambda instance. 

### Languages Available in Lambda

* Node.js
* Java
* Python
* C#
* Go

### Lambda Pricing

* **Charged on number of requests**
    * First 1 million requests per month are free, then $0.20 per 1 million over
* **Duration of execution**
    * calculated from the beginning of code execution until the termination, rounded up to the nearest 100ms. 
    * pricing depends on the amount of memory you allocate. You are charged **$0.00001667 for every GB-second** used.

### Lambda Version Control

With **versioning** in lambda, you can publish one or more versions of your lambda function. This way you can work with different version in your development worklow, such as beta and prod. 

Each lambda function version has a unique Amazon Resource Name (ARN). It cannot be changed after your function is published. 

#### Qualified/Unqualified ARNs

* **Qualified**: The function ARN with a version suffix
    * arn:aws:lambda:aws-region:account-id:function:helloworld:**$LATEST**
* **Unqualified**: The function ARN without a version suffix
    * arn:aws:lambda:aws-region:account-id:function:helloworld

#### Aliases

A name that points to a particular version of your function, eg version 1. Then update $LATEST but the aliases still references version 1 until you manually promite the alias to version 2. 

### Exam Tips

* Lambda scales **out** NOT **up**
* Lambda functions are independent. 1 event == 1 function invocation
* Lambda is serverless
* Lambda functions can trigger other lambda functions. 
* AWS X-Ray allows you to debug lambda architecture
* Lambda is global
* Know your trigger types
* Can split traffig using alaises to different versions of lambda functions

## API Gateway


**API** is an Application Programming Interface. **Amazon API Gateway** is a fully managed service for developers to mantain, monitor, and secure APIs at any scale. This acts as a front door for applications to access data, business logic, or other functionality from your back end services. 

![API Gateway Diagram](./images/api-gateway-arch.png)

### What can API Gateway do?

* Expose HTTPS endpoints to define a RESTful API
* Serverlessly connect to services like Lambda and DynamoDB
* Send each API endpoint to a different target (service)
* Scales easily
* Track and control usage by API key
* Throttle requests
* Cloudwatch integration for logging and monitoring
* Maintain multiple versions of your API

### Configuring an API Gateway

* Define an API (container)
* Define resources and nested resources (URL paths)
* For each resource:
    * select supported HTTP method (eg. GET)
    * set security
    * choose a target (EC2, Lambda, etc)
    * configure request and response transformations
* Deploy an API to a stage
    * Uses API gateway domain by default
    * can use custom domain
    * supports AWS cert manager for free SSL/TLS certs

### API Caching

* Reduces the amount of calls by caching responses that originated from the same request. 
* Responses are cached for a specified time-to-live (TTL) in seconds
* If a response is in the cache, rather than hitting the endpoint API gateway will respond from the cache instead.

### Same Origin Policy

Important for web application security. Under this policy, web browsers permit script contained in a primary web page to access data in an additional web page, but only if both pages have the same origin. 

This is done to prevent cross sit scripting attacks. It is enforced by web browsers, but ignored by tools like CURL. 

### Cross Origin Resource Sharing

Is used for relaxing the same origin policy. CORS is a mechanism that allows restricted resources on a web page to be requested from another domain outside the domain from which the resource was served. 

### Exam Tips

* API gateway can use caching to increase performance
* API gateway is low cost and scales automatically 
* throttling API gateway is an option to prevent attacks
* logging can be enabled via CloudWatch
* If using JS/AJAX with multiple domains through API gateway, make sure to enable CORS on API gateway. 
* CORS is enforced by the client. 

## Advanced API Gateway

You can use **API Gateway Import** to import an API from an external definition file into API Gateway. It currently supports **Swagger v2** definitions. 

You can:
* **Create** an API by submitting a **POST** request that includes a Swagger defintion
* **Update** an API by submitting a **PUT** request that contains a swagger defintion in the payload. 
    * You can update an API via overwrite or merge an API with an existing API

### API Throttling

* By default, API gateay limits the steady-state request rate to 10,000 requests per second (rps). 
* Max 5000 concurrent requests across all APIs within an AWS account
    * If you exceed 5,000 concurrent or 10,000 rps, the API will return a **429 too many requests** error response. 

## Step Functions

Allow you to visualize and test serverless functions. They provide a graphical console to arrange and visualize the components of your application as a series of steps, making it simple to build and run multistep applications. 

They automatically trigger and track each step, retrying when there are errors, so that your applications executes in the expected order. In each step, the state is logged for diagnostic purposes. 

### Sequential Step Functions

![Sequential Step Function](./images/sequential-step-function.png)

### Branching Step Functions

![Branching Step Function](./images/branch-step-function.png)

### Parallel Step Functions

![Parallel Step Functions](./images/parallel-step-function.png)

## AWS X-Ray

X-Ray is a service that collects data about requests that your application serves, and provides tools you can use to view, filter, and gain insights into that data to identify issues and opportunities for optimization. 

For any traced request, you can see detailed information about the request and response, calls that your application makes to downstream AWS resources, microservices, databases, and Web APIs. 

### X-Ray Architecture 

1. You must have the AWS X-Ray SDK installed in your application
2. SDK sends data to **X-Ray Daemon** which then sends the data to the X-Ray API.
3. X-Ray console takes that data and makes a dashboard out of it. 
4. The **scrips and tools** either communicate directy with the daemon or with the API

![X-Ray Architecture](./images/x-ray-arch.png)

### X-Ray SDK

#### The SDK provides
* Interceptors to add to your code to trace incoming HTTP traffic
* Client handlers to instrument AWS SDK clients that your application uses to call other AWS services
* and HTTP client to use to instrument calls to other internal or external HTTP web services

#### The SDK integrates with
* Elastic Load Balancer
* Lambda
* API Gateway
* EC2
* Elastic Beanstalk

#### SDK Languages
* Java
* Go
* Node.js
* Python
* Ruby
* .NET

