# Simple Notification Service

## SNS 101

SNS is a web service that makes it easy to set up, operate, and send notifications from the cloud. 

It provides a mechanism that is highly scaleable, flexible, and cost effective capability to publish messages from an applications and immidiatelly deliver them to subscribers and applications. 

Includes push notifications to:

* Apple
* Google
* Fire OS
* Windows
* Android in China via Baidu Cloud Push
* SMS
* Email
* SQS
* Http Endpoints

And SNS can also trigger Lambda functions. 

## Topics

Topics are a way to group multiple recipients. A **topic** is an access point for allowing recipients to dynamically subscribe for identical copies of the same notification. 

Topics can group delivery methods. A single topic for exmaple could deliver to:

* Android
* iOS
* SMS

When you publish once to a topic, SNS delivers appropiately formatted copies of your message to each subscriber. 

## Benefits

* Instantaneos, push based delivery (no polling)
* Simple APIs and easy application integration
* multiple transport protocalls
* Pay as you go model with no up front costs
* AWS management console

## Pricing

* **$0.50** per 1 million SNS requests
* **$0.06** per 100,000 deliveries over HTTP
* **$0.75** per 100 deliveries over SMS
* **$2.00** per 100,000 deliveries over email

## Architecture

SNS operates on a publisher-subscriber model