##### AWS Certifications
AWS Certifications will give you knowledge of Cloud Computing and the different computing resources available in the 
Cloud. There are three different AWS certifications that would help in acquiring this initial fluency:

![AWS Certifications](https://d1.awsstatic.com/training-and-certification/cert-roadmap/AWS_Certification_Roadmap_April_2018.d51f56ef22f8d98ad54423c132a976eab2b94abf.png)

1. [AWS Certified Solutions Architect Associate Guide](./aws-associate-architect/README.md)
2. AWS Certified Developer
    * [Exam Tips](https://captechventuresinc.sharepoint.com/sites/SO/Cloud/Documents/AWS%20Certified%20Developer%20-%20exam%20notes.docx)
    * AWS Certified Associate Developer - [Study Notes](./aws-associate-dev/README.md)
3. AWS Certified SysOps Administrator

Any certificate from the Associate section would be great to have and will give you a better understanding of DevOps 
and Cloud Computing. I would recommend first studying for the Solutions Architect exam and passing it. It is a difficult 
exam but would make any subsequent Associate-level certification much easier to grab afterwards.

###### Picking the right training class
To pass the exams more quickly, use the services provided by [ACloudGuru](https://acloud.guru/courses?vendors=aws). Ryan 
has built an entire business by staying on top of the latest cloud services from all of the major vendors including AWS,
Google Cloud Platform and Microsoft's Azure.

Pick the exam you want to tackle and purchase the course from ACloudGuru. ACloudGuru also publishes their classes on 
Udemy - so you may be able to find an Associate certification course for cheaper there.

###### Progressing through the class
The most important part of passing these certifications is going through the tutorials, step-by-step, on your own. You 
need to understand what the different AWS services do at a high-level and the proper ways to use them in order to solve
a real-world problem.

Get comfortable with the AWS console and AWS command line tools.

Also, each AWS service you need to learn about has certain facts that must be memorized. For example, when I took the 
exam, S3 had a default bucket limit of 100 buckets max per account. Amazon will throw a few of these types of questions 
on the exam. I recommend using Quizlet or flashcards to quiz and recall this information many times before your exam.

###### Take practice exams
ACloudGuru also offers practice exams that are very similar to the actual exam on [Udemy](https://www.udemy.com/aws-certified-developer-associate-2017-practice-tests/). The actual exam will most definitely be 
harder, but the practice exams are pretty close and are the best way to figure out if your ready. 

Once you have watched all of the videos from the training class, try to take one of the practice exams on your own. You 
should be able to score greater than a 90% on the practice exams before you feel comfortable signing up for the actual 
certification exam.