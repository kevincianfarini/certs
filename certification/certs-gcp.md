# Google Cloud Platform

## Cloud Architect

The 6 part Coursera courses: [GCP Architecture](https://www.coursera.org/specializations/gcp-architecture)
 
Cloud Solutions Architecture Reference [site](http://gcp.solutions/)
 
Practice exam Google Form it has about 25 sample questions, see the [practice exam](https://cloud.google.com/certification/practice-exam/cloud-architect). The actual exam has 50. 
 
For more information, please contact @kjamieson or @acampbell
