# Microsoft Azure Role Based Certification

[Main Azure Certification Page](https://www.microsoft.com/en-us/learning/browse-new-certification.aspx)

## Microsoft Azure Fundamentals

[Certification Page](https://www.microsoft.com/en-us/learning/azure-fundamentals.aspx)

This is an excellent exam for anyone who is just getting started with Microsoft Azure.

By taking this exam, you can prove that you understand cloud concepts, core Azure Services, Azure pricing and support, and the fundamentals of cloud security, privacy, compliance, and trust.

### Exams Needed To Attain The Certification

[AZ-900](https://www.microsoft.com/en-us/learning/exam-AZ-900.aspx) Microsoft Azure Fundamentals

## Microsoft Certified Azure Administrator [Associate]

[Certification Page](https://www.microsoft.com/en-us/learning/azure-administrator.aspx)

This certification used to be [70-533](https://www.microsoft.com/en-us/learning/exam-70-533.aspx).

### Exams Needed To Attain The Certification

[AZ-103](https://www.microsoft.com/en-us/learning/exam-AZ-103.aspx)  Microsoft Azure Administrator  

**UPGRADE OFFER EXPIRES JUNE 30, 2019**  
If you have passed the 70-533 exam, take a single upgrade exam:  
[AZ-102](https://www.microsoft.com/en-us/learning/exam-AZ-102.aspx) Microsoft Azure Administrator Certification Transition


## Microsoft Certified Azure Developer [Associate]

[Certification Page](https://www.microsoft.com/en-us/learning/azure-developer.aspx)

This certification used to be [70-532](https://www.microsoft.com/en-us/learning/exam-70-532.aspx)

### Exam Needed To Attain The Certification

[AZ-203](https://www.microsoft.com/en-us/learning/exam-AZ-203.aspx) Developing Solutions for Microsoft Azure  


## Microsoft Certified Azure Solutions Architect [Expert]

[Certification page](https://www.microsoft.com/en-us/learning/azure-solutions-architect.aspx)

This certification used to be [70-535](https://www.microsoft.com/en-us/learning/exam-70-535.aspx)

### Exams Needed To Attain The Certification

[AZ-300](https://www.microsoft.com/en-us/learning/exam-AZ-300.aspx) Microsoft Azure Architect Technologies  
[AZ-301](https://www.microsoft.com/en-us/learning/exam-AZ-301.aspx) Microsoft Azure Architect Design  

**- OR -**

**UPGRADE OFFER EXPIRES JUNE 30, 2019**  
If you have passed the 70-535 exam, take a single upgrade exam:  
[AZ-302](https://www.microsoft.com/en-us/learning/exam-AZ-302.aspx) Microsoft Azure Solutions Architect Certification Transition 


## Microsoft Certified Azure DevOps Engineer [Expert]

[Certification page](https://www.microsoft.com/en-us/learning/azure-devops.aspx)

### Exams Needed To Attain The Certification

[AZ-400](https://www.microsoft.com/en-us/learning/exam-az-400.aspx) Microsoft Azure DevOps Solutions  
