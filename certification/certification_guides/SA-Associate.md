# Path to AWS Solutions Architect - Associate Certification

## Valuable Resources

1. [ACloudGuru Course](https://acloud.guru/learn/aws-certified-solutions-architect-associate) ($99)
    - Gives a great explantion of all the different services AWS has to offer
    - Comes with a 18 hours of video lectures, quizzes, and a practice exam
        - Note: Practice exam is helpful but does not emulate the format of the exam well
    - The [ACloudGuru forums](https://acloud.guru/forums/aws-certified-solutions-architect-associate/recent?p=1) are a great place to ask questions and learn from the experience/insight of former exam takers

2. [Essential AWS White Papers](https://www.one-tab.com/page/KwSLg-DUQqS5fIvGYysP2A)  
    - A lot of this can be repeated information from the ACloudGuru course but there are certain details that will only be found in these whitepapers

3. [Essential AWS FAQs](https://www.one-tab.com/page/Rhn5L8VqT_ya_phqPPP8lQ)  
    - A lot of this can be repeated information from the ACloudGuru course but there are certain details that will only be found in these FAQs

4. [AWS Offical Practice exam](https://www.aws.training/certification?src=certification) ($20)
    - You can sign up for a practice exam much like you would a regular exam
    - Not necessary, but helps you understand the format of the questions/exam to come and tells you what areas of the exam you might be weak in

5. Blogs/Other Helpful Resources
    - [Jayendra Patil Blog](http://jayendrapatil.com/aws-solutions-architect-associate-feb-2018-exam-learning-path/)
        - Highly reccomend this blog! Comes with detailed write ups of all the services and really good sample exam questions!
        - If you do not understand an answer to one of the sample questions, do not memorize the questions look up the reasoning behind it
    - [CapTech Notes/Tips](https://captechventuresinc.sharepoint.com/sites/SO/Cloud/Documents/AWS%20Certified%20Solutions%20Architect%20-%20exam%20notes.docx)

6. After crushing the exam
    - Congratulations!!
    - Submit your expenses for the exam, ACloudGuru course, practice exam, and any other approved resources
    - Add your certification to the [CapTech Certification Tracker](https://captechventuresinc.sharepoint.com/sites/PA/SI/Lists/Certification%20Tracking/AllItems.aspx) 
        - $100 Gift Card (As of October 2018)
    - Continue to add onto this document any suggestions or resources you used to succeed on your exam

## What I felt unprepared for on the exam

- Detailed questions about the read/write scalability of DynamoDB