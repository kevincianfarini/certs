# Getting started with DevOps and Cloud

<img src="https://marketplace-cdn.atlassian.com/s/public/devops-hero-1-87966cfbc9c5713ae047551c7b22985c.png" width="50%" height="50%" />

Getting started in DevOps and the Cloud can be overwhelming due to the high number of tools and technologies. This page is not a complete reference but should provide a few pointers to support your learning journey.  Please feel free to submit a Pull Request to update it with your experience and feedback to continue helping others!

Need help? Please reach to @ZHughes or @RBortoloto.

- [Getting started with DevOps and Cloud](#getting-started-with-devops-and-cloud)
	- [Prerequisites](#prerequisites)
		- [Python](#python)
		- [Bash](#bash)
	- [Cloud](#cloud)
	- [Continuous Integration and Delivery with Jenkins](#continuous-integration-and-delivery-with-jenkins)
	- [Configuration Management with Ansible](#configuration-management-with-ansible)
	- [Containerization with Docker](#containerization-with-docker)
		- [Container Orchestration with Kubernetes](#container-orchestration-with-kubernetes)
- [Appendix](#appendix)
- [DevOps Reading List](#devops-reading-list)

## Prerequisites
Two scripting languages that are used very often are Python and Bash. It is very important to feel comfortable
with both of them.
In addition, it is very important to have a basic understanding of Networking fundamentals. If you need to brush up on 
this information, check out [Networking Concepts and Protocols](https://app.pluralsight.com/library/courses/comptia-network-plus-networking-concepts/table-of-contents).

### Python
Complete the
[Data Engineering Python challenge](https://captechventuresinc.sharepoint.com/sites/SO/DI/_layouts/15/guestaccess.aspx?guestaccesstoken=vOjSAiXSuc5U69VpFg8ph8GdjCk05VRaC4RRXeE1Tk0%3d&docid=2_1864028b082854495917e4ced83c453d6&rev=1)
and find a reason to build something with this language on your own. [Flask](http://flask.pocoo.org/) is a great Python microframework for
getting a restful API<sub>1</sub> off the ground very quickly. Build something with it!

### Bash
To get more familiarity with Bash, complete the
[Data Engineering scripting challenge](https://captechventuresinc.sharepoint.com/sites/SO/DI/_layouts/15/guestaccess.aspx?guestaccesstoken=1W2sIDQjS09k39S40cj2zgfvCadnlczesRsFF5BPEhI%3d&docid=2_140209998c70149c196f1f8f5fd052f68&rev=1)
and start using your terminal as much as you can. Start writing bash scripts to automate some of the boring stuff you
need to do from time-to-time.

## Cloud

* Infrastructure as a Service [Tech Challenge](https://captechventuresinc.sharepoint.com/sites/SO/Cloud/Cloud%20Tech%20Challenge/Cloud%20Tech%20Challenge.docx)
  
A great way to get up-to-speed on current best practices in the Cloud is to study available training materials for a 
Cloud certification. The most mature Cloud certification program is AWS. ACloudGuru has great reference material that 
gives you a comprehensive overview of the different services available today. Please refer to the [certification](../certification/README.md) 
section for more information.

## Continuous Integration and Delivery with Jenkins
An open-source automation server written in Java. Jenkins helps to automate the non-human part of the software 
development process, helping to facilitate continuous integration and continuous delivery.

* [DevOps Tech Challenge #1](https://captechventuresinc.sharepoint.com/:w:/r/sites/SO/DevOps/DevOps%20TechChallenges/DevOps-Tech-Challenge-Java-1.docx?d=wc2ab1d0e3fd8462ba27d94cea9e092f2&csf=1)
* [DevOps Tech Challenge #2](https://captechventuresinc.sharepoint.com/:w:/r/sites/SO/DevOps/DevOps%20TechChallenges/DevOps-Tech-Challenge-Java-2.docx?d=w1e415a63d7c348c98374d35f17f8122c&csf=1)

## Containerization with Docker

The world's leading software containerization platform. Docker enables true independence between applications and infrastructure 
and developers and IT ops to unlock their potential and creates a model for better collaboration and innovation.

* [Getting Started with Docker](https://app.pluralsight.com/library/courses/docker-getting-started/table-of-contents)
* [Docker Deep Dive](https://www.pluralsight.com/courses/docker-deep-dive-update)

*Work in Progress: Looking for reviewers of the Beta version*

* [Docker Tech Challenge #1](https://techchallenge.captechlab.com/cloud/Container.Assignment-1.Docker-Fundamentals)
* [Docker Tech Challenge #2](https://techchallenge.captechlab.com/cloud/Container.Assignment-2.Docker-Advanced)
* [Docker Tech Challenge and Amazon ECS](https://techchallenge.captechlab.com/cloud/Container.Assignment.Docker-AWS-ECS)

### Container Orchestration with Kubernetes

Written in [GoLang](https://golang.org) by Google. Donated to [Cloud Native Computing Foundation](https://www.cncf.io/) in 2014.
Allows you to automate the deployment, scaling and management of containerized applications.

* [Getting Started with Kubernetes](https://app.pluralsight.com/library/courses/getting-started-kubernetes/table-of-contents)

## Configuration Management with Ansible

Open-source software that automates software provisioning, configuration management, and application deployment.

* [Hands-on Ansible](https://app.pluralsight.com/library/courses/hands-on-ansible/table-of-contents)

# Appendix
1. If you have time, please read the original dissertation, "[Architectural Styles and the Design of Network-based 
Software Architectures](https://www.ics.uci.edu/~fielding/pubs/dissertation/top.htm)" by Roy Fielding. In it, "the 
Godfather of REST" outlines the idea of Representational State Transfer (REST) in a digestible manner. It's over 100 
pages but is well worth the time for a more nuanced understanding of an oft-abused pattern.

# DevOps Reading List
1. [The DevOps Handbook](https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations/dp/1942788002)
2. [The Phoenix Project](http://itrevolution.com/book/the-phoenix-project/)

# Additional Resources
1. Check out "[Taking JavaEE to the Cloud](https://youtu.be/9H1EZ0SwKBM)," a presentation delivered by CapTech's own Rodrigo Bortoloto and Reza Rahman 
   at JavaOne 2017. In it, Rodrigo runs you through the different ways of deploying a JavaEE application in the Cloud; 
   specifically - he shows you to how to: 
     * deploy your application onto an EC2 instance
     * dockerize your app and deploy onto an ECS cluster
     * use PaaS "Jelastic" to deploy your app into the Cloud