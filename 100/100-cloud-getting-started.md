# Intro to Cloud Computing

## CapTech Partnerships:

* AWS [AWS Partner Network](https://partnercentral.awspartner.com/home)
* Azure
* Google Cloud Platform - evaluating

## Intro to the Cloud

* [Cloud Computing: The Big Picture](https://app.pluralsight.com/library/courses/cloud-computing-big-picture/table-of-contents)

## Intro to AWS

### AWS Partner Training

The AWS partnership provides the following training, among many others:

  * [AWS TCO and Cloud Economics](https://www.aws.training/learningobject/curriculum?id=10743)
  * [AWS Business Professional Accreditation](https://www.aws.training/learningobject/curriculum?id=11289)
  * [AWS Technical Professional Accreditation](https://www.aws.training/learningobject/curriculum?id=11276)
